//
// Copyright (c) 2018~2018 by xuzhao9 <i@xuzhao.net>
// This file is part of HoshiDict.
//
// HoshiDict is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// HoshiDict is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with HoshiDict; see the file COPYING. If not,
// see <http://www.gnu.org/licenses/>.
//
extern crate byteorder;
extern crate inflate;

use std::str;
use std::fs;
use std::mem::size_of;
use std::path::Path;
use std::io;
use std::io::{Error, ErrorKind};
use std::collections::HashMap;
use self::byteorder::{ReadBytesExt, LittleEndian};
use super::ahd;

pub enum LingoesDType {
  AHD,
}

pub struct Lingoes {
  dict_path: String,
  ld2: Vec<u8>,
  inflated_ld2: Vec<u8>,
  pos: i32,
  inflated_pos: i32,
  magic: String,
  version: (i16, i16),
  summary: i32,
  dict: HashMap<String, String>,
  dtype: LingoesDType,
}

pub enum DefFormat {
  XML,
  TEXT,
  JSON,
}

impl Lingoes {
  fn get_version(self: &Lingoes) -> (i16, i16) {
    let x = &self.ld2[0x18..0x18+2];
    let major = io::Cursor::new(x).read_i16::<LittleEndian>().unwrap();
    let x = &self.ld2[0x1A..0x1A+2];
    let minor = io::Cursor::new(x).read_i16::<LittleEndian>().unwrap();
    (major, minor)
  }

  fn get_header_length(self: &Lingoes) -> i32 {
    let x = &self.ld2[0x5C..0x5C+4];
    let offset = io::Cursor::new(x).read_i32::<LittleEndian>().unwrap();
    offset + 0x60
  }

  fn get_i32(arr: &Vec<u8>, start: i32) -> i32 {
    let start = start as usize;
    let x = &arr[start..(start + size_of::<i32>())];
    io::Cursor::new(x).read_i32::<LittleEndian>().unwrap()
  }

  fn gen_error(reason: &str) -> Result<Lingoes,io::Error> {
    return Err(Error::new(ErrorKind::Other, reason));
  }

  fn initialize(mut self: Lingoes) -> io::Result<Lingoes> {
    {
      let magic = str::from_utf8(&self.ld2[0..4]).expect("UTF8 Error");
      if !(magic=="?LD2" || magic=="?LDX") {
        return Lingoes::gen_error("Invalid dictionary magic number");
      }
      self.magic = String::from(magic);
    } {
      let (major, minor) = self.get_version();
      self.version = (major, minor);
    } {
      let hdr_sz = self.get_header_length();
      if self.ld2.len() > (hdr_sz as usize) {
        self.summary = hdr_sz as i32;
        let dtype = Lingoes::get_i32(&self.ld2, hdr_sz);
        if dtype != 3 {
          return Lingoes::gen_error("File contains a non-local dictionary.");
        }
      } else {
        return Lingoes::gen_error("File does not contain a dictionary.");
      }
    }
    return Ok(self);
  }

  fn get_idx(self: &mut Lingoes, pos: i32) -> Vec<i32> {
    let mut out = Vec::new();
    let i32size = size_of::<i32>() as i32;
    self.inflated_pos = pos;
    out.push(Lingoes::get_i32(&self.inflated_ld2, self.inflated_pos));
    self.inflated_pos += i32size;
    out.push(Lingoes::get_i32(&self.inflated_ld2, self.inflated_pos));
    self.inflated_pos += i32size;
    out.push((self.inflated_ld2[self.inflated_pos as usize] as i32) & 0xff);
    self.inflated_pos += 1;
    out.push((self.inflated_ld2[self.inflated_pos as usize] as i32) & 0xff);
    self.inflated_pos += 1;
    out.push(Lingoes::get_i32(&self.inflated_ld2, self.inflated_pos));
    self.inflated_pos += i32size;
    out.push(Lingoes::get_i32(&self.inflated_ld2, self.inflated_pos));
    self.inflated_pos += i32size;
    out
  }

  fn read_definition(self: &mut Lingoes, word_offset: i32, xml_offset: i32, datalen: i32, index: i32) -> (String, String) {
    let mut idxdata = self.get_idx(datalen * index);
    let mut last_word_pos = idxdata[0];
    let mut last_xml_pos = idxdata[1];
    let mut refs = idxdata[3];
    let current_word_offset = idxdata[4];
    let mut current_xml_offset = idxdata[5];
    let mut xml = String::from(str::from_utf8(&self.inflated_ld2[(xml_offset + last_xml_pos)as usize..(xml_offset+current_xml_offset)as usize])
                           .expect("Failed to decode from utf8!"));
    while refs > 0 {
      refs -= 1;
      let r = Lingoes::get_i32(&self.inflated_ld2, word_offset + last_word_pos);
      idxdata = self.get_idx(datalen * r);
      last_xml_pos = idxdata[1];
      current_xml_offset = idxdata[5];
      let extend_xml = str::from_utf8(&self.inflated_ld2[(xml_offset + last_xml_pos)as usize..(xml_offset+current_xml_offset)as usize]).expect("Failed to decode from utf8!");
      if xml.is_empty() {
        xml = String::from(extend_xml);
      } else {
        xml = format!("{}, {}", extend_xml, xml);
      }
      last_word_pos += 4;
    }
    let word = str::from_utf8(&self.inflated_ld2[(word_offset+last_word_pos) as usize..(word_offset+current_word_offset) as usize])
      .expect("Failed to decode from utf8");
    (String::from(word), xml)
  }
  
  fn extract(self: &mut Lingoes, def_offset: i32, xml_offset: i32) {
    let datalen = 10;
    let def_total = (def_offset / datalen) - 1;
    for i in 0..def_total {
      let (voc, def) = self.read_definition(def_offset, xml_offset, datalen, i);
      self.dict.insert(voc, def);
    }
    println!("Successfully loaded {} definitions.", def_total);
  }

  fn inflate(self: &Lingoes, deflate_stream: Vec<i32>) -> Vec<u8> {
    let start = self.pos;
    let mut offset = -1;
    let mut inflated_data: Vec<u8> = Vec::new();
    let mut last_offset = start;
    for relative_offset in deflate_stream {
      offset = start + relative_offset;
      let chunk_start = last_offset;
      let chunk_len = offset - last_offset;
      let x = &self.ld2[chunk_start as usize..(chunk_start+chunk_len) as usize];
      let decoded = inflate::inflate_bytes_zlib(x).unwrap();
      inflated_data.extend_from_slice(&decoded);
      last_offset = offset;
    }
    inflated_data
  }
  
  // the dict starts from Lingoes::meta::summary
  // *start: dict_type
  // *start+4: dict_length
  // *start+8: compressed dataheader length
  // *start+0x0C: inflated word index length
  // *start+0x10: inflated words length // 0x10
  // *start+0x14: inflated xml length // 0x14 
  // *start+0x18: unknown    // 0x18
  // *start+0x1C: index starts   // *(index_offset)
  // *start+compressed_datahdr_offset : *(compressed_dataheader_offset)
  // compressed_data_header
  // position: compressed_dataheader_offset + 8
  pub fn load(self: &mut Lingoes) -> Result<(), &str> {
    let start = self.summary; 
    let limit = start + Lingoes::get_i32(&self.ld2, start + 4) + 8; // *start == dtype, *(start+4) == length
    let index_offset = start + 0x1C;
    let compressed_dataheader_offset = Lingoes::get_i32(&self.ld2, start+8) + index_offset;
    let inflated_words_index_length = Lingoes::get_i32(&self.ld2, start+12);
    let inflated_words_length = Lingoes::get_i32(&self.ld2, start+16);
    let inflated_xml_length = Lingoes::get_i32(&self.ld2, start+20);
    let definitions = (compressed_dataheader_offset - index_offset) / 4;
    self.pos = compressed_dataheader_offset + 8;
    let mut offset = Lingoes::get_i32(&self.ld2, self.pos);
    self.pos += size_of::<i32>() as i32;

    let mut deflate_stream: Vec<i32> = Vec::new();
    while offset + self.pos < limit {
      offset = Lingoes::get_i32(&self.ld2, self.pos);
      // println!("pos: {} , offset: {}, limit: {}", self.pos, offset, limit);
      deflate_stream.push(offset);
      self.pos += size_of::<i32>() as i32;
    }

    let compressed_data_offset = self.pos; // header is done
    // println!("Index numbers: {}", definitions);
    // println!("Index address: 0x{:x}, size: {}",
    //          index_offset, compressed_dataheader_offset - index_offset);
    // println!("Compressed data address: 0x{:x}, size: {}",
    //          compressed_data_offset, (limit - compressed_data_offset));
    // println!("Decompressed word index address: 0x0, size: {}",
    //          inflated_words_index_length);
    // println!("Decompressed word address: 0x{:x}, size: {}",
    //          inflated_words_index_length, inflated_words_length);
    // println!("Decompressed xml address: 0x{:x}, size: {}",
    //          inflated_words_index_length + inflated_words_length, inflated_xml_length);
    // println!("Decompressed file size: {} KB",
    //          (inflated_words_index_length + inflated_words_length + inflated_xml_length)/1024);
    
    self.inflated_ld2 = self.inflate(deflate_stream);
    if !(self.inflated_ld2.len() == 0) {
      self.pos = index_offset + (size_of::<i32>() as i32) * definitions;
      self.extract(inflated_words_index_length,
                   inflated_words_index_length + inflated_words_length);
      Ok(())
    } else {
      Err("Failed to deflate dictionary.")
    }
  }
  
  // open a file, verify its correctness. Return a LingoesV2 object if ok.
  pub fn open(dpath: &str, dtype: LingoesDType) -> io::Result<Lingoes> {
    let p = Path::new(dpath);
    let content = fs::read(p)?;
    let out = Lingoes {
      dict_path: dpath.to_string(),
      ld2: content,
      inflated_ld2: Vec::new(),
      pos: 0,
      inflated_pos: 0,
      magic: Default::default(),
      version: Default::default(),
      summary: 0,
      dict: HashMap::new(),
      dtype: dtype,
    };
    return Lingoes::initialize(out);
  }
  
  pub fn search(self: &Lingoes, word: String) -> Option<&String> {
    self.dict.get(&word)
  }
  
  pub fn purge(self: &Lingoes, def: String) -> String {
    match self.dtype {
      LingoesDType::AHD => {
        ahd::purge(def)
      },
      _ => {
        def
      }
    }
  }
}
