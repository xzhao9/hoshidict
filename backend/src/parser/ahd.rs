//
// Copyright (c) 2018~2018 by xuzhao9 <i@xuzhao.net>
// This file is part of HoshiDict.
//
// HoshiDict is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// HoshiDict is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with HoshiDict; see the file COPYING. If not,
// see <http://www.gnu.org/licenses/>.
//

fn is_preserve_start(s: &String, ind: usize) -> bool {
  let n = s.chars().nth(ind+1).unwrap();
  if n == 'g' {
    return true;
  }
  false
}

fn is_preserve_over(s: &String, ind: usize) -> bool {
  let n1 = s.chars().nth(ind+1).unwrap();
  let n2 = s.chars().nth(ind+2).unwrap();
  if n1 == '/' && n2 == 'g' {
    return true;
  }
  false
}

fn is_newline(s: &String, ind: usize) -> bool {
  let n = s.chars().nth(ind+1).unwrap();
  if n == 'n' {
    return true;
  }
  false
}

// Remove redundant parts of the definition
pub fn purge(def: String) -> String {
  let mut ndef = String::new();
  let mut i = 0;
  let mut preserve = false;
  loop {
    if i >= def.chars().count() - 1 {
      break;
    }
    let cur = def.chars().nth(i).unwrap();
    if cur == '<' { // deal with tags
      if is_preserve_start(&def, i) {
        preserve = true;
      } else if is_preserve_over(&def, i) {
        preserve = true;
      } else if is_newline(&def, i) {
        ndef.push('\n');
      }
      loop { // loop through until the next '>'
        i += 1;
        let next = def.chars().nth(i).unwrap();
        if next == '>' {
          break;
        }
      }
    } else {
      if preserve {
        ndef.push(cur);
      }
    }
    i += 1;
  }
  ndef
}

#[test]
fn purge_works() {
  let s = format!("<C><F><H /><I><N><g>ap·ple</g>(ăp<Ë M=\"dict://res/i_3F981FE6.png\" G=\"4\" H=\"12\" />əl)<n /><h>n.</h><n /><Ë M=\"dict://res/i_D9294167.gif\" /> <Ë M=\"dict://res/i_A19943D6.gif\" /> A deciduous Eurasian tree <h>(Malus pumila)</h> having alternate simple leaves and white or pink flowers. <Ë M=\"dict://res/i_A19943D6.gif\" /> The firm, edible, usually rounded fruit of this tree.<n /> <Ë M=\"dict://res/i_D9294167.gif\" /> <Ë M=\"dict://res/i_A19943D6.gif\" /> Any of several other plants, especially those with fruits suggestive of the apple, such as the crab apple or custard apple. <Ë M=\"dict://res/i_A19943D6.gif\" /> The fruit of any of these plants.<n /><h><g>Idiom:</g></h><n /><g><x K=\"#0000FF\"><h>apple of (one's) eye</h></x></g><n />One that is treasured: <x K=\"#0000FF\"><h>Her grandson is the apple of her eye.</h> </x> <u />[Middle English <k> appel</k>, from Old English<k> æppel</k>.]</N></I></F></C>");
  let s2 = format!("<C><F><H /><I><N><g>A·sian</g>(ā<Ë M=\"dict://res/i_3F981FE6.png\" G=\"4\" H=\"12\" />zhən, ā<Ë M=\"dict://res/i_3F981FE6.png\" G=\"4\" H=\"12\" />shən)<n /><h>adj.</h><n />Of or relating to Asia or its peoples, languages, or cultures.<n /><h>n.</h><n /><Ë M=\"dict://res/i_D9294167.gif\" /> A native or inhabitant of Asia.<n /><Ë M=\"dict://res/i_D9294167.gif\" /> A person of Asian descent.<n /><x K=\"#006000\"><h><g>Usage Note: </g></h>Asia is the largest of the continents with more than half the world's population. Though strictly speaking all of its inhabitants are <h>Asians,</h> in practice this term is applied almost exclusively to the peoples of East, Southeast, and South Asia as opposed to those of Southwest Asia<Ë M=\"dict://res/i_08993081.png\" G=\"6\" H=\"8\" />such as Arabs, Turks, Iranians, and Kurds<Ë M=\"dict://res/i_08993081.png\" G=\"6\" H=\"8\" />who are more usually designated <h>Middle</h> or <h>Near Easterners.</h> Indonesians and Filipinos are properly termed <h>Asian,</h> since their island groups are considered part of the Asian continent, but not the Melanesians, Micronesians, and Polynesians of the central and southern Pacific, who are now often referred to collectively as <h>Pacific Islanders.</h> See Usage Note at <Y O=\"Oriental\">Oriental</Y>.</x></N></I></F></C>");
  println!("{}", purge(s));
  println!("{}", purge(s2));
  assert_eq!(1+2, 3);
}
