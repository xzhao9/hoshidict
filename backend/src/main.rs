//
// Copyright (c) 2018~2018 by xuzhao9 <i@xuzhao.net>
// This file is part of HoshiDict.
//
// HoshiDict is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// HoshiDict is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with HoshiDict; see the file COPYING. If not,
// see <http://www.gnu.org/licenses/>.
//
extern crate linefeed;

mod parser;
use parser::ld2::Lingoes;
use parser::ld2::LingoesDType;
use std::io::prelude::*;
use std::io::{self, BufRead};

fn main() {
  // let mut ahd = match Lingoes::open("../dicts/M-W.ld2") {
  // let mut ahd = match Lingoes::open("../dicts/GRE.ld2") {
  let mut ahd = match Lingoes::open("../dicts/American.Heritage.Dictionary.ld2",
                                    LingoesDType::AHD) {
    Ok(ld2) => {
      ld2
    },
    Err(err) => {
      panic!("Error ... {}", err);
    }
  };
  ahd.load().expect("Failed to load dict");
  loop {
    let stdin = io::stdin();
    print!("hoshi-dict> ");
    io::stdout().flush().unwrap();
    let line = match stdin.lock().lines().next() {
      Some(l) => l,
      None => {
        println!("");
        break;
      }
    }.expect("Can't read from the next line.");
    let def = match ahd.search(line) {
      Some(def) => {
        def
      },
      None => {
        "Definition not found."
      }
    };
    println!("{}", ahd.purge(def.to_string())); 
  }
}
