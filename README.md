# HoshiDict

## Frontend

The frontend of HoshiDict is written in C++ and Qt GUI toolkit.

## Backend

The backend of HoshiDict is written in Rust to avoid memory bugs.

Currently, it only supports the following dictionaries in ld2 format:

- American Heritage Dictionary
