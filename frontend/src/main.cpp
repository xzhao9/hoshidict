#include "mainwindow.hpp"

#include <QApplication>
#include <QDir>

int main(int argc, char *argv[]) {
    QApplication hoshi(argc, argv);
    MainWindow w;
    w.show();
    return hoshi.exec();
}
